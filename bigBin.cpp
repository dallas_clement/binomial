#include <cmath>
#include <iostream>
#include <ios>
#include <vector>
#include <cassert>
#include <algorithm>

double body(long long y, long long x, long long i, double powBase, double mult, long long yRx)
{
	double bodyResult = 1;
	bodyResult *= mult*(y-i)/6.0/double(x-i);
	if (i < yRx)
	{
		bodyResult*= powBase;
	}
	return bodyResult;
}


double bigBin(long long y, long long x)
{
	double res = 1;
	long long yFx = (y-x)/x;
	double powBase = 5.0/6.0;
	double mult = std::pow(powBase, yFx);
	long long yRx = (y-x) % x;

	long long head = 0;
	long long tail = x-1;
	while( head <= tail)
	{
		double headFact = body(y,x, head, powBase, mult, yRx);
		double tailFact = body(y,x, tail, powBase, mult, yRx);
		double headResult = headFact*res;
		double tailResult = tailFact*res;
		double headSize = headResult < 1 ? 1/headResult : headResult;
		double tailSize = tailResult < 1 ? 1/tailResult : tailResult;
		if ( headSize < tailSize)
		{
			res = headResult;
		   ++head;
		}
		else
		{
		   res = tailResult;
			--tail;
		}
	}
	return res;
}

double stirBody(long long y, long long x)
{
	assert(y > x && x != 0);
	double yMxBase = 5.0*y/(6.0*(y-x));
	double yMxPower = (y-x)/x;
	double yMx = std::pow(yMxBase, yMxPower);
	double xBase = y*yMx/(6.0*x);
	double pi = std::atan(1)*4;
	double frontFactor = std::pow(y/( (y-x) * x * 2.0 * pi), 0.5);
	double a = std::pow(xBase*yMxBase, (y-x)%x); 
	double b = std::pow(xBase, x - ((y-x)%x));
	return frontFactor * a*b;
}

double bigStir(long long x)
{
    double pi = std::atan(1)*4;
	 return std::pow(6.0/(5.0*x*2*pi),0.5);
}

void probRange(long long y, long long mostProbable, std::vector<double> probabilityThresholds) //, std::ifstream outstream) 
{
	long long rangeEnd = mostProbable, rangeStart = mostProbable;
	double maxThreshold = *std::max_element(probabilityThresholds.begin(), probabilityThresholds.end());

	double totalProbability = stirBody(y, mostProbable);
	double nextHigherValue = stirBody(y, rangeEnd+1);
   double nextLowerValue = stirBody(y, rangeStart-1);

	auto nextThresholdIterator = std::min_element(probabilityThresholds.begin(), probabilityThresholds.end());
	double nextThreshold = *nextThresholdIterator;
	
	while (totalProbability <= maxThreshold)
	{
		if ( ( nextHigherValue >= nextLowerValue && rangeEnd != y) || rangeStart == 0)
		{
			totalProbability += nextHigherValue;
			++rangeEnd;
			nextHigherValue = stirBody(y, rangeEnd+1);	
		}
		else
		{
			totalProbability += nextLowerValue;
			--rangeStart;
			nextLowerValue = stirBody(y, rangeStart-1);
		}
		if (totalProbability > nextThreshold)
		{
			std::cout << std::scientific << rangeStart << " " 
	                                   << mostProbable - rangeStart << " "
                                      << rangeEnd << " " 
	                                   << rangeEnd - mostProbable << " "
                                      <<  totalProbability << std::endl;
		   assert(nextThresholdIterator != probabilityThresholds.end() );
			probabilityThresholds.erase(nextThresholdIterator);
			nextThresholdIterator = std::min_element(probabilityThresholds.begin(), probabilityThresholds.end());
			nextThreshold = *nextThresholdIterator;
		}
	}

}


int main(){
	long long int y(6000000000);
	long long int x(1000000000);
	
	std::vector<double> thresholds { 0.00001, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99997857 };
	probRange(y,x,thresholds);


	return 0;
}
